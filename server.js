// Required modules
var http = require('http'),
    express = require('express'),
    app = express(),
    path = require('path'),
    mongoose = require('mongoose'),
    morgan = require('morgan');

// Define what folder our app should use 
app.use(express.static(__dirname + '/public'));

// Define our view engine
app.engine('.html', require('ejs').__express);
app.set('view engine', 'html');

// Define where to look for our views
app.set('views', __dirname + '/public/views');

// Log all requests
app.use(morgan('dev'));

// Render the index page
app.get('/', function(req, res) {
  res.render(path.resolve(__dirname + '/public/index.html'));
});

// Define port to listen at
app.listen(8080);
console.log('Listening on port 8080');
